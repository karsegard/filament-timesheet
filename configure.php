#!/usr/bin/env php
<?php

function ask(string $question, string $default = ''): string
{
    $answer = readline($question . ($default ? " ({$default})" : null) . ': ');

    if (!$answer) {
        return $default;
    }

    return $answer;
}

function confirm(string $question, bool $default = false): bool
{
    $answer = ask($question . ' (' . ($default ? 'Y/n' : 'y/N') . ')');

    if (!$answer) {
        return $default;
    }

    return strtolower($answer) === 'y';
}

function writeln(string $line): void
{
    echo $line . PHP_EOL;
}

function run(string $command): string
{
    return trim((string) shell_exec($command));
}

function str_after(string $subject, string $search): string
{
    $pos = strrpos($subject, $search);

    if ($pos === false) {
        return $subject;
    }

    return substr($subject, $pos + strlen($search));
}

function slugify(string $subject): string
{
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $subject), '-'));
}

function title_case(string $subject): string
{
    return str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $subject)));
}

function title_snake(string $subject, string $replace = '_'): string
{
    return str_replace(['-', '_'], $replace, $subject);
}

function replace_in_file(string $file, array $replacements): void
{
    $contents = file_get_contents($file);

    file_put_contents(
        $file,
        str_replace(
            array_keys($replacements),
            array_values($replacements),
            $contents
        )
    );
}

function remove_prefix(string $prefix, string $content): string
{
    if (str_starts_with($content, $prefix)) {
        return substr($content, strlen($prefix));
    }

    return $content;
}

function remove_composer_deps(array $names)
{
    $data = json_decode(file_get_contents(__DIR__ . '/composer.json'), true);

    foreach ($data['require-dev'] as $name => $version) {
        if (in_array($name, $names, true)) {
            unset($data['require-dev'][$name]);
        }
    }

    foreach ($data['require'] as $name => $version) {
        if (in_array($name, $names, true)) {
            unset($data['require'][$name]);
        }
    }

    file_put_contents(__DIR__ . '/composer.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
}

function remove_composer_script($scriptName)
{
    $data = json_decode(file_get_contents(__DIR__ . '/composer.json'), true);

    foreach ($data['scripts'] as $name => $script) {
        if ($scriptName === $name) {
            unset($data['scripts'][$name]);
            break;
        }
    }

    file_put_contents(__DIR__ . '/composer.json', json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE));
}

function remove_readme_paragraphs(string $file): void
{
    $contents = file_get_contents($file);

    file_put_contents(
        $file,
        preg_replace('/<!--delete-->.*<!--\/delete-->/s', '', $contents) ?: $contents
    );
}
function remove_empty_subfolders($path)
{
    $empty = true;
    foreach (glob($path . DIRECTORY_SEPARATOR . "*") as $file) {
        $empty &= is_dir($file) && remove_empty_subfolders($file);
    }
    return $empty && (is_readable($path) && count(scandir($path)) == 2) && rmdir($path);
}

function get_comments($filename)
{

    $docComments = array_filter(token_get_all(file_get_contents($filename)), function ($entry) {
        return $entry[0] == T_COMMENT;
    });
    return $docComments;
}

$used_php_segments = [];
function uncomment_php_segment(string $file, string $key): void
{
    global $used_php_segments;
    if (!isset($used_php_segments[$file])) {
        $used_php_segments[$file] = [];
    }
    $contents = file_get_contents($file);
    $comments = get_comments($file);
    $re = "/\/\*" . $key . "\n?(\s?.*\s?)\n?" . $key . "\*\//s";
    $used_php_segments[$file][] = $key;
    foreach ($comments as $comment) {
        $c = $comment[1];
        if (preg_match($re, $c, $res)) {
            $contents = str_replace($res[0], $res[1], $contents);
        }
    }
    file_put_contents(
        $file,
        $contents
    );
}
function cleanup_php_segments($file)
{
    global $used_php_segments;
    $contents = file_get_contents($file);
    $comments = get_comments($file);
    //var_dump($used_php_segments);
    $re = '/\/\*([^\s]*)\n?(\s?.*\s?)\n?(\1)\*\//s';
    foreach ($comments as $comment) {
        $c = $comment[1];
        if (preg_match($re, $c, $res)) {
            if (count($res) == 4 && !empty($res[1])) {
                //       var_dump('checking');
                $segments = $used_php_segments[$file] ?? [];
                if ($res[1] == $res[3] && !in_array($res[1], $segments)) {
                    //        var_dump('removing');
                    //       var_dump($res);
                }
                $contents = str_replace($res[0], '', $contents);
            }
        }
    }

    $lines = explode("\n", $contents);
    $contents = "";
    foreach ($lines as $line) {
        $_line = trim($line);
        if (!empty($_line)) {
            $contents .= $line . "\n";
        }
    }

    file_put_contents(
        $file,
        $contents
    );
}
//uncomment_php_segment('./src/SkeletonServiceProvider.php','HasConfig');
//cleanup_php_segments('./src/SkeletonServiceProvider.php');
//die('dead');
function safeUnlink(string $filename)
{
    if (file_exists($filename) && is_file($filename)) {
        unlink($filename);
    }
}

function determineSeparator(string $path): string
{
    return str_replace('/', DIRECTORY_SEPARATOR, $path);
}

function replaceForWindows(): array
{
    return preg_split('/\\r\\n|\\r|\\n/', run('dir /S /B * | findstr /v /i .git\ | findstr /v /i vendor | findstr /v /i ' . basename(__FILE__) . ' | findstr /r /i /M /F:/ ":author :vendor :package VendorName skeleton migration_table_name vendor_name vendor_slug author@domain.com"'));
}

function replaceForAllOtherOSes(): array
{
    return explode(PHP_EOL, run('grep -E -r -l -i ":author|:vendor|:package|VendorName|skeleton|migration_table_name|vendor_name|vendor_slug|author@domain.com" --exclude-dir=vendor ./*  | grep -v ' . basename(__FILE__)));
}


$gitName = run('git config user.name');
$authorName = ask('Author name', $gitName);

$gitEmail = run('git config user.email');
$authorEmail = ask('Author email', $gitEmail);

$usernameGuess = explode(':', run('git config remote.origin.url'))[1];
$usernameGuess = dirname($usernameGuess);
$usernameGuess = basename($usernameGuess);
$authorUsername = ask('Author username', $usernameGuess);

$vendorName = ask('Vendor name', $authorUsername);
$vendorSlug = slugify($vendorName);
$vendorNamespace = ucwords($vendorName);
$vendorNamespace = ask('Vendor namespace', $vendorNamespace);

$hasLibrary = false ;
$libraryName = "";
$facadeName = "";
$hasLibrary = confirm('Create Library',$hasLibrary);
if($hasLibrary){
    $libraryName = ask('Library Name');
    $facadeName = ask('Facade Name',$libraryName);
}
$createModel = false;
$createModel = confirm('Create Model', $createModel);
$modelName = "";
if ($createModel) {
    $modelName =  ask('Model name', 'Model');
}

$createEloquentFeature = confirm('Create Eloquent feature',false);
$eloquentFeatureName ="";
if($createEloquentFeature){
    $eloquentFeatureName =  ask('Eloquent feature name', 'HasFeature');

}

$createInstallCommand = confirm('Create Install Command',false);

$createMigration = $createModel;
$createModel = confirm('Create Migration', $createMigration);

$filamentPackage = false;
$filamentPackage = confirm('Create Filament Provider', $filamentPackage);

$currentDirectory = getcwd();
$folderName = basename($currentDirectory);

$packageName = ask('Package name', $folderName);
$packageSlug = slugify($packageName);
$packageSlugWithoutPrefix = remove_prefix('laravel-', $packageSlug);

$packageNamespace = title_case($packageName);
$packageNamespace = ask('Package Namespace name', $packageNamespace);
$escapedPackageNamespace =  str_replace("\\","\\\\",$packageNamespace);
$variableName = lcfirst($packageNamespace);
$description = ask('Package description', "This is my package {$packageSlug}");


/*
$usePhpStan = confirm('Enable PhpStan?', true);
$usePhpCsFixer = confirm('Enable PhpCsFixer?', true);
$useDependabot = confirm('Enable Dependabot?', true);
$useLaravelRay = confirm('Use Ray for debugging?', true);
$useUpdateChangelogWorkflow = confirm('Use automatic changelog updater workflow?', true);
*/
$migrationDate = date('Y_m_d_his');

writeln('------');
writeln("Author     : {$authorName} ({$authorUsername}, {$authorEmail})");
writeln("Vendor     : {$vendorName} ({$vendorSlug})");
writeln("Package    : {$packageSlug} <{$description}>");
writeln("Namespace  : {$vendorNamespace}\\{$packageNamespace}");
writeln("Class name : {$packageNamespace}");
writeln("---");

if(!$createInstallCommand){
    safeUnlink('./src/Commands/InstallCommand.php');
}

$hasConfig = confirm('Package has config', false);
$hasYamlConfig = false;
if($hasConfig){
    mkdir('./config/' . $vendorSlug);
    $hasYamlConfig=confirm('Package use yaml config', $hasYamlConfig);
}


/*writeln("Packages & Utilities");
writeln("Use PhpCsFixer       : " . ($usePhpCsFixer ? 'yes' : 'no'));
writeln("Use Larastan/PhpStan : " . ($usePhpStan ? 'yes' : 'no'));
writeln("Use Dependabot       : " . ($useDependabot ? 'yes' : 'no'));
writeln("Use Ray App          : " . ($useLaravelRay ? 'yes' : 'no'));
writeln("Use Auto-Changelog   : " . ($useUpdateChangelogWorkflow ? 'yes' : 'no'));
writeln('------');
*/
writeln('This script will replace the above values in all relevant files in the project directory.');



if (!confirm('Modify files?', true)) {
    exit(1);
}


if (!$hasConfig) {
    safeUnlink('./config/skeleton.php');
    remove_composer_deps(['pragmarx/yaml']);
   // rmdir('./config');
} else {
    
    if ($hasYamlConfig) {
        uncomment_php_segment('./src/ServiceProvider.php', 'HasYamlConfig');
        
    }else{
        remove_composer_deps(['pragmarx/yaml']);
    }
    uncomment_php_segment('./src/ServiceProvider.php', 'HasConfig');
}

if($hasLibrary===true){
    uncomment_php_segment('./src/ServiceProvider.php', 'HasFacade');
}else{
    safeUnlink('./src/LibraryName.php');
    safeUnlink('./src/Facades/FacadeName.php');

}

if ($filamentPackage) {
    uncomment_php_segment('./src/ServiceProvider.php', 'HasFilamentProvider');
} else {
    safeUnlink('./src/FilamentServiceProvider.php');
    remove_composer_deps(["spatie/laravel-package-tools"]);
}

if (!$createModel) {
    safeUnlink('./src/Models/ModelName.php');
    safeUnlink('./database/factories/ModelNameFactory.php');
   // rmdir('./src/Models');
   // rmdir('./database/factories');
}

if (!$createMigration) {
    safeUnlink('./database/migrations/migration.php');
  //  rmdir('./database/migrations');
}else{
    uncomment_php_segment('./src/ServiceProvider.php', 'HasLoadableMigration');

}
if(!$createEloquentFeature){
    safeUnlink('./src/Models/Traits/EloquentFeatureName.php');
}

cleanup_php_segments('./src/ServiceProvider.php');

$files = (str_starts_with(strtoupper(PHP_OS), 'WIN') ? replaceForWindows() : replaceForAllOtherOSes());



foreach ($files as $file) {
    replace_in_file($file, [
        ':author_name' => $authorName,
        ':author_username' => $authorUsername,
        'author@domain.com' => $authorEmail,
        ':vendor_name' => $vendorName,
        ':vendor_slug' => $vendorSlug,
        'ModelName' => $modelName,
        'VendorName' => $vendorNamespace,
        ':package_name' => $packageName,
        ':package_slug' => $packageSlug,
        ':package_slug_without_prefix' => $packageSlugWithoutPrefix,
        'EscapedSkeleton' =>$escapedPackageNamespace,
        'Skeleton' => $packageNamespace,
        'skeleton' => $packageSlug,
        'migration_table_name' => title_snake($packageSlug),
        'variable' => $variableName,
        ':package_description' => $description,
        'EloquentFeatureName'=>$eloquentFeatureName,
        'LibraryName'=>$libraryName,
        'FacadeName'=>$facadeName
    ]);
    //mkdir('./config/'.$vendorSlug);
    echo $file;
    match (true) {
        str_contains($file, determineSeparator('config/skeleton.php')) => rename($file, determineSeparator('./config/' . $vendorSlug . '/' . $packageSlugWithoutPrefix . '.php')),
        $createModel && str_contains($file, determineSeparator('src/Models/ModelName.php')) => rename($file, determineSeparator('./src/Models/' . $modelName . '.php')),
        $hasLibrary && str_contains($file, determineSeparator('src/LibraryName.php')) => rename($file, determineSeparator('./src/' . $libraryName . '.php')),
        $hasLibrary && str_contains($file, determineSeparator('src/Facades/FacadeName.php')) => rename($file, determineSeparator('./src/Facades/' . $facadeName . '.php')),
        $createModel && str_contains($file, determineSeparator('database/factories/ModelNameFactory.php')) => rename($file, determineSeparator('./database/factories/' . $modelName . 'Factory.php')),
        str_contains($file, determineSeparator('src/Skeleton.php')) => rename($file, determineSeparator('./src/' . $packageNamespace . '.php')),
        //str_contains($file, determineSeparator('src/SkeletonServiceProvider.php')) => rename($file, determineSeparator('./src/' . $packageNamespace . 'ServiceProvider.php')),
        //$filamentPackage && str_contains($file, determineSeparator('src/SkeletonFilamentServiceProvider.php')) => rename($file, determineSeparator('./src/' . $packageNamespace . 'FilamentServiceProvider.php')),
        str_contains($file, determineSeparator('src/Facades/Skeleton.php')) => rename($file, determineSeparator('./src/Facades/' . $packageNamespace . '.php')),
        str_contains($file, determineSeparator('src/Commands/SkeletonCommand.php')) => rename($file, determineSeparator('./src/Commands/' . $packageNamespace . 'Command.php')),
        $createMigration && str_contains($file, determineSeparator('database/migrations/migration.php')) => rename($file, determineSeparator('./database/migrations/' . $migrationDate . '_create_' . title_snake($packageSlugWithoutPrefix) . '_table.php')),
        $createEloquentFeature && str_contains($file, determineSeparator('src/Models/Traits/EloquentFeatureName.php')) => rename($file, determineSeparator('./src/Models/Traits/'.$eloquentFeatureName.'.php')),
        str_contains($file, 'README.md') => remove_readme_paragraphs($file),
        default => [],
    };
}
remove_empty_subfolders(".");

/*
if (! $usePhpCsFixer) {
    safeUnlink(__DIR__ . '/.php_cs.dist.php');
    safeUnlink(__DIR__ . '/.github/workflows/php-cs-fixer.yml');
}

if (! $usePhpStan) {
    safeUnlink(__DIR__ . '/phpstan.neon.dist');
    safeUnlink(__DIR__ . '/phpstan-baseline.neon');
    safeUnlink(__DIR__ . '/.github/workflows/phpstan.yml');

    remove_composer_deps([
        'phpstan/extension-installer',
        'phpstan/phpstan-deprecation-rules',
        'phpstan/phpstan-phpunit',
        'nunomaduro/larastan',
    ]);

    remove_composer_script('phpstan');
}

if (! $useDependabot) {
    safeUnlink(__DIR__ . '/.github/dependabot.yml');
    safeUnlink(__DIR__ . '/.github/workflows/dependabot-auto-merge.yml');
}

if (! $useLaravelRay) {
    remove_composer_deps(['spatie/laravel-ray']);
}

if (! $useUpdateChangelogWorkflow) {
    safeUnlink(__DIR__ . '/.github/workflows/update-changelog.yml');
}
*/
confirm('Execute `composer install` and run tests?',false) && run('composer install && composer test');

confirm('Let this script delete itself?', true) && unlink(__FILE__);
