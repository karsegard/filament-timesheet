<?php

namespace VendorName\Skeleton\Models\Traits;


trait EloquentFeatureName
{

    public static function bootEloquentFeatureName(): void
    {
        static::creating(function ($model)
        {
        });
        static::updating(function ($model)
        {
        });
    }

/*
    public function has_many_polymorphic()
    {
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
