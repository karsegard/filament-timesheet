<?php

namespace VendorName\Skeleton\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class ModelName extends Model
{
    use HasFactory;

    protected $fillable = [
        
    ];

    protected $appends = [
        
    ];

    protected $casts = [
       
    ];

   
    protected static function newFactory()
    {
        return  \VendorName\Skeleton\Database\Factories\ModelNameFactory::new();
    }

}
